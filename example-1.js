import {createAll, cleanConsole} from './data';
const companies = createAll();

cleanConsole(1, companies);
console.log('---- EXAMPLE 1 --- ', 'Put here your function');

// -----------------------------------------------------------------------------
// INSTRUCCIONES EN ESPAÑOL

// Crear una función tomando la variable "companies" como parámetro y reemplazando
// todos los valores "undefined" en "usuarios" por un string vacío.
// El nombre de cada "company" debe tener una letra mayúscula al principio, así como
// el apellido y el nombre de cada "user".
// Las "companies" deben ordenarse por su total de "user" (orden decreciente)
// y los "users" de cada "company" deben aparecer en orden alfabético.

// -----------------------------------------------------------------------------
// INSTRUCTIONS IN ENGLISH

// Create a function taking the variable "companies" as a parameter and replacing
// all values "undefined" in "users" by an empty string.
// The name of each "company" must have a capital letter at the beginning as well as
// the last name and first name of each "user".
// The "companies" must be sorted by their number of "user" (decreasing order)
// and the "users" of each "company" must be listed in alphabetical order.

// -----------------------------------------------------------------------------
// INSTRUCTIONS EN FRANÇAIS

// Créer une fonction prenant en paramètre la variable "companies" et remplaçant
// toutes les valeurs "undefined" dans les "users" par un string vide.
// Le nom de chaque "company" doit avoir une majuscule au début ainsi que
// le nom et le prénom de chaque "user".
// Les "companies" doivent être triées par leur nombre de "user" (ordre décroissant)
// et les "users" de chaque "company" doivent être classés par ordre alphabétique.
export function exemple1(companies) {
    
   const result = company.map( (company)=> { 
                    company.name = capi(company.name);
                    company.users = company.users.map((user)=> {
                       user.firstName === undefined?user.firstName = '': user.firstName = capi(user.firstName);
                       user.lastName === undefined?user.lastName = '':user.lastName = capi(user.lastName);
                        return value2
                  }).sort(compare);
       return companies
   });
    
    return result.sort((a,b)=>a.users.length - b.users.length)
    
}

function capi(str) {
let res = str.replace( str[0],str[0].toUpperCase());
return res
}

 function compare (a, b){
    var x = a.lastName;
    var y = b.lastName;
    if (x < y) {return -1;}
    if (x > y) {return 1;}
    return 0;
  }